﻿// variables
window.categoriesPath = "http://services.odata.org/Northwind/Northwind.svc/Categories";
window.productsPath = "http://services.odata.org/Northwind/Northwind.svc/Products";
window.suppliersPath = "http://services.odata.org/Northwind/Northwind.svc/Suppliers";

var northwindRouter = new kendo.Router({
    init: function () {
        layoutView.render("#application");
    }
});

var layoutView = new kendo.Layout("layout-template");

var index = new kendo.View("index-template", { model: indexModel });

var indexModel = kendo.observable({
    onSelect: function (e) {
        var menuItem = $(e.item).children(".k-link").text().toLowerCase();
        northwindRouter.navigate("/" + menuItem);
    }
});

var categoryDataSource = new kendo.data.DataSource({
    transport: {
        read: { url: window.categoriesPath, dataType: "json" }
    },
    schema: {
        data: "value"
    }
});

var categoriesModel = kendo.observable({
    items: categoryDataSource
});
var categoriesView = new kendo.View("categories-template", { model: categoriesModel });

northwindRouter.route("/categories", function () {
    layoutView.showIn("#pre-content", index);
    layoutView.showIn("#content", categoriesView);
});

$(function() {
    northwindRouter.start();
    northwindRouter.navigate("/categories");
});



